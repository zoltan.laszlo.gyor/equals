package main;

public class Main {
	public static void main(String[] args) {
		String text1 = new String("Hey man");
		String text2 = new String("Hey man");
		String text3 = text1;
		
		System.out.println(text1 == text2);
		System.out.println(text1.equals(text2));
		System.out.println(text1 == text3);
		System.out.println(text3.equals(text1));
	}
}
